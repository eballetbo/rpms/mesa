#!/bin/sh
set -x
# Usage: ./make-git-snapshot.sh [COMMIT]
#
# to make a snapshot of the given tag/branch.  Defaults to HEAD.
# Point env var REF to a local mesa repo to reduce clone time.

HEAD=${1:-$( cat commitid )}
DIRNAME=mesa-$( date +%Y%m%d )git${1:-$( cat commitid )}

echo REF ${REF:+--reference $REF}
echo DIRNAME $DIRNAME
echo HEAD $HEAD

rm -rf $DIRNAME

git clone ${REF:+--reference $REF} \
	https://gitlab.freedesktop.org/StaticRocket/mesa.git $DIRNAME

GIT_DIR=$DIRNAME/.git git archive --format=tar --prefix=$DIRNAME/ $HEAD \
	| xz > $DIRNAME.tar.xz

# rm -rf $DIRNAME
